package com.avellum.translateman.talk;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.avellum.translateman.R;

import java.lang.reflect.Array;
import java.util.Locale;
import java.util.Timer;

public class TalkActivity extends AppCompatActivity implements View.OnClickListener{

    private String spanishWord;
    private String englishWord;
//    private String ukrainianWord;
//    private String russianWord;

    private TextToSpeech textToSpeechES;
    private TextToSpeech textToSpeechEN;
//    private TextToSpeech textToSpeechUK;
//    private TextToSpeech textToSpeechRU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_talk);

        TextView es = (TextView) findViewById(R.id.spanish_text_view);
        TextView en = (TextView) findViewById(R.id.english_text_view);
//        TextView uk = (TextView) findViewById(R.id.ukrainian_text_view);
//        TextView ru = (TextView) findViewById(R.id.russian_text_view);

        Intent i = getIntent();
        spanishWord = i.getStringExtra("spanish");
        englishWord = i.getStringExtra("english");
//        ukrainianWord = i.getStringExtra("ukrainian");
//        russianWord = i.getStringExtra("russian");

        es.setText("español: " + spanishWord);
        en.setText("english: " + englishWord);
//        uk.setText("українська: " + ukrainianWord);
//        ru.setText("русский: " + russianWord);

        new Thread(new Runnable() {
            @Override
            public void run() {
                textToSpeechES = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            Locale es = new Locale("es", "US");
                            textToSpeechES.setLanguage(es);
//                } else {
//                    Snackbar.make(, R.string.word_added, Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
                        }
                    }
                });
            }
        }).run();

        new Thread(new Runnable() {
            @Override
            public void run() {
                textToSpeechEN = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            textToSpeechEN.setLanguage(Locale.ENGLISH);
//                } else {
//                    Snackbar.make(v, R.string.word_added, Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
                        }
                    }
                });
            }
        }).run();


//        textToSpeechUK = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status != TextToSpeech.ERROR) {
//                    Locale uk = new Locale("uk", "UA");
//                    textToSpeechES.setLanguage(uk);
////                } else {
////                    Snackbar.make(v, R.string.word_added, Snackbar.LENGTH_LONG)
////                            .setAction("Action", null).show();
//                }
//            }
//        });
//
//        textToSpeechRU = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int status) {
//                if (status != TextToSpeech.ERROR) {
//                    Locale ru = new Locale("ru", "RU");
//                    textToSpeechES.setLanguage(ru);
////                } else {
////                    Snackbar.make(v, R.string.word_added, Snackbar.LENGTH_LONG)
////                            .setAction("Action", null).show();
//                }
//            }
//        });

        ImageButton esButton = (ImageButton) findViewById(R.id.spanish_talk_button);
        esButton.setOnClickListener(this);

        ImageButton enButton = (ImageButton) findViewById(R.id.english_talk_button);
        enButton.setOnClickListener(this);

//        ImageButton ukButton = (ImageButton) findViewById(R.id.ukrainian_talk_button);
//        ukButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textToSpeechUK.speak(ukrainianWord, TextToSpeech.QUEUE_FLUSH, null);
//            }
//        });
//
//        ImageButton ruButton = (ImageButton) findViewById(R.id.russian_talk_button);
//        ruButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textToSpeechRU.speak(russianWord, TextToSpeech.QUEUE_FLUSH, null);
//            }
//        });
    }

    @Override
    public void onPause() {
        if (textToSpeechES != null) {
            textToSpeechES.stop();
            textToSpeechES.shutdown();
        }
        if (textToSpeechEN != null) {
            textToSpeechEN.stop();
            textToSpeechEN.shutdown();
        }
//        if (textToSpeechUK != null) {
//            textToSpeechUK.stop();
//            textToSpeechUK.shutdown();
//        }
//        if (textToSpeechRU != null) {
//            textToSpeechRU.stop();
//            textToSpeechRU.shutdown();
//        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (textToSpeechES != null) {
            textToSpeechES.stop();
            textToSpeechES.shutdown();
        }
        if (textToSpeechEN != null) {
            textToSpeechEN.stop();
            textToSpeechEN.shutdown();
        }
//        if (textToSpeechUK != null) {
//            textToSpeechUK.stop();
//            textToSpeechUK.shutdown();
//        }
//        if (textToSpeechRU != null) {
//            textToSpeechRU.stop();
//            textToSpeechRU.shutdown();
//        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.spanish_talk_button:
                textToSpeechES.speak(spanishWord, TextToSpeech.QUEUE_FLUSH, null);
                break;
            case R.id.english_talk_button:
                textToSpeechEN.speak(englishWord, TextToSpeech.QUEUE_FLUSH, null);
                break;
        }
    }
}
