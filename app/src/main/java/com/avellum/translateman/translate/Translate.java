package com.avellum.translateman.translate;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by niklpod on 29.01.16, 15:49
 */
public class Translate {
    private static String API_KEY = "trnsl.1.1.20160126T221421Z.0e51eb6800283cba.6f7827ecc75e0ea7ed73378e72191c9a4f06ed97";

    public ArrayList<String> translate(String text) {
        AsyncTask<String, String, ArrayList<String>> parse = new Parse()
                .execute(
                        yandexJSON(text, "es"),
                        yandexJSON(text, "en"),
                        yandexJSON(text, "uk"),
                        yandexJSON(text, "ru")
                );
        ArrayList<String> result = null;
        try {
            result = parse.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String yandexJSON(String text, String languageCode) {
        return String.format("https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=%s", API_KEY, text, languageCode);
    }

    private class Parse extends AsyncTask<String, String, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> result = new ArrayList<>();
            for (String jsonString : params) {
                try {
                    URL url = new URL(jsonString);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                    // Check the connection status
                    if (urlConnection.getResponseCode() == 200) {
                        // if response code = 200 ok
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                        // Read the BufferedInputStream
                        BufferedReader r = new BufferedReader(new InputStreamReader(in));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = r.readLine()) != null) {
                            sb.append(line);
                        }
                        JSONObject jObj = new JSONObject(sb.toString());
                        JSONArray array = jObj.getJSONArray("text");
                        result.add(array.getString(0));

                        // Disconnect the HttpURLConnection
                        urlConnection.disconnect();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }
    }
}
