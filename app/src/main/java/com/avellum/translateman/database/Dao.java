package com.avellum.translateman.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by niklpod on 29.01.16, 15:36
 */
public class Dao {
    // Database fields
    private SQLiteDatabase database;
    private Database dbHelper;
    private String[] allColumns = {
            Database.COLUMN_ID,
            Database.COLUMN_WORD_IN_SPANISH,
            Database.COLUMN_WORD_IN_ENGLISH,
            Database.COLUMN_WORD_IN_UKRAINIAN,
            Database.COLUMN_WORD_IN_RUSSIAN,
            Database.COLUMN_PATH
    };

    public Dao(Context context) {
        dbHelper = new Database(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Word createWord(String... word) {
        ContentValues values = new ContentValues();
        values.put(Database.COLUMN_WORD_IN_SPANISH, word[0]);
        values.put(Database.COLUMN_WORD_IN_ENGLISH, word[1]);
        values.put(Database.COLUMN_WORD_IN_UKRAINIAN, word[2]);
        values.put(Database.COLUMN_WORD_IN_RUSSIAN, word[3]);
        values.put(Database.COLUMN_PATH, word[4]);
        long insertId = database.insert(Database.TABLE_WORDS, null, values);
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                Database.COLUMN_ID + " = " + insertId,
                null,
                null,
                null,
                null
        );
        cursor.moveToFirst();
        Word newWord = cursorToWord(cursor);
        cursor.close();
        return newWord;
    }

    public void deleteWord(Word word) {
        long id = word.getId();
        database.delete(
                Database.TABLE_WORDS,
                Database.COLUMN_ID + " = " + id,
                null
        );
    }

    public boolean searchWord(String word) {
        boolean b = true;
        Cursor c = database.query(
                Database.TABLE_WORDS,
                null,
                "(" + Database.COLUMN_WORD_IN_SPANISH + " like '" + word + "')",
                null,
                null,
                null,
                null
        );
        if (c.moveToLast()) {
            b = false;
        }
        c.close();
        return b;
    }

    public List<Word> getAllWords() {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                null
        );
        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToPrevious();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    public List<Word> getAllWordsFromPath(String path) {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                path
        );
        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToPrevious();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    public List<Word> sortByEnglishWords() {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                Database.COLUMN_WORD_IN_ENGLISH
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    public List<Word> sortBySpanishWords() {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                Database.COLUMN_WORD_IN_SPANISH
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    public List<Word> sortByUkrainianWords() {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                Database.COLUMN_WORD_IN_UKRAINIAN
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    public List<Word> sortByRussianWords() {
        List<Word> words = new ArrayList<>();
        Cursor cursor = database.query(
                Database.TABLE_WORDS,
                allColumns,
                null,
                null,
                null,
                null,
                Database.COLUMN_WORD_IN_RUSSIAN
        );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Word word = cursorToWord(cursor);
            words.add(word);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return words;
    }

    private Word cursorToWord(Cursor cursor) {
        Word word = new Word();
        word.setId(cursor.getLong(0));
        word.setSpanish(cursor.getString(1));
        word.setEnglish(cursor.getString(2));
        word.setUkrainian(cursor.getString(3));
        word.setRussian(cursor.getString(4));
        return word;
    }
}
