package com.avellum.translateman.database;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by niklpod on 29.01.16, 15:36
 */
public class Word {
    private long id;
    private String spanish;
    private String english;
    private String ukrainian;
    private String russian;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpanish() {
        return spanish;
    }

    public void setSpanish(String spanish) {
        this.spanish = spanish;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getUkrainian() {
        return ukrainian;
    }

    public void setUkrainian(String ukrainian) {
        this.ukrainian = ukrainian;
    }

    public String getRussian() {
        return russian;
    }

    public void setRussian(String russian) {
        this.russian = russian;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return "español: " + this.spanish
                + "\nenglish: " + this.english
                + "\nукраїнська: " + this.ukrainian
                + "\nрусский: " + this.russian
                + "\npath: " + this.path;
    }
}
