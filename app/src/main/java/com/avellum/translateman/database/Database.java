package com.avellum.translateman.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by niklpod on 29.01.16, 15:36
 */
public class Database extends SQLiteOpenHelper {
    public static final String TABLE_WORDS = "words";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_WORD_IN_SPANISH = "spanish";
    public static final String COLUMN_WORD_IN_ENGLISH = "english";
    public static final String COLUMN_WORD_IN_UKRAINIAN = "ukrainian";
    public static final String COLUMN_WORD_IN_RUSSIAN = "russian";
    public static final String COLUMN_PATH = "path";

    private static final String DATABASE_NAME = "words.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table " + TABLE_WORDS + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_WORD_IN_SPANISH + " text, "
            + COLUMN_WORD_IN_ENGLISH + " text, "
            + COLUMN_WORD_IN_UKRAINIAN + " text, "
            + COLUMN_WORD_IN_RUSSIAN + " text, "
            + COLUMN_PATH + " text);";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(Database.class.getName(),
                "Upgrading database from version " + oldVersion
                        + " to " + newVersion
                        + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORDS);
        onCreate(db);
    }
}
