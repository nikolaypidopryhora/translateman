package com.avellum.translateman.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.avellum.translateman.R;
import com.avellum.translateman.database.Word;

import java.util.List;

/**
 * Created by niklpod on 10.02.16, 2:24
 */
public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Word> wordsItems;

    public CustomListAdapter(Activity activity, List<Word> wordsItems) {
        this.activity = activity;
        this.wordsItems = wordsItems;
    }

    @Override
    public int getCount() {
        return wordsItems.size();
    }

    @Override
    public Object getItem(int location) {
        return wordsItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_list_adapter, null);

        TextView wordES = (TextView) convertView.findViewById(R.id.wordES);
        TextView wordEN = (TextView) convertView.findViewById(R.id.wordEN);
        TextView wordUK = (TextView) convertView.findViewById(R.id.wordUK);
        TextView wordRU = (TextView) convertView.findViewById(R.id.wordRU);

        Word c = wordsItems.get(position);
        wordES.setText(c.getSpanish());
        wordEN.setText(c.getEnglish());
        wordUK.setText(c.getUkrainian());
        wordRU.setText(c.getRussian());

        return convertView;
    }

    public void remove(Word word) {
        wordsItems.remove(word);
        notifyDataSetChanged();
    }

    public void insert(Word word, int i) {
        wordsItems.add(i, word);
        notifyDataSetChanged();
    }

    public void clear() {
        wordsItems.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Word> typeOfSort) {
        wordsItems.addAll(typeOfSort);
        notifyDataSetChanged();
    }

    public int getPosition(Word word) {
        return wordsItems.indexOf(word);
    }
}
