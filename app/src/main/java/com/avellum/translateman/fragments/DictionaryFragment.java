package com.avellum.translateman.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import com.avellum.translateman.R;
import com.avellum.translateman.adapter.CustomListAdapter;
import com.avellum.translateman.database.Dao;
import com.avellum.translateman.database.Word;
import com.avellum.translateman.talk.TalkActivity;
import com.avellum.translateman.translate.Translate;

import org.droidparts.widget.ClearableEditText;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by niklpod on 29.01.16, 15:36
 */
public class DictionaryFragment extends Fragment implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, View.OnClickListener, AdapterView.OnItemSelectedListener {
    private ClearableEditText editText;
    private ListView listView;
    private Word word = null;
    private Dao database;
    private ArrayAdapter<Word> adapter;
    private CustomListAdapter customListAdapter;
    private Spinner sort;
    private ArrayAdapter<CharSequence> sortAdapter;
    private List<Word> typeOfSort;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dictionary_fragment, container, false);
        // open database
        database = new Dao(getActivity());
        try {
            database.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // get all words from database
        typeOfSort = database.getAllWords();

        // use the SimpleCursorAdapter to show the elements in a ListView
        customListAdapter = new CustomListAdapter(getActivity(), typeOfSort);
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setAdapter(customListAdapter);
        listView.setSelection(0);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        sort = (Spinner) view.findViewById(R.id.sortSpinner);
        sort.setOnItemSelectedListener(this);

        sortAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort, android.R.layout.simple_spinner_item);
        sortAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sort.setAdapter(sortAdapter);

        editText = (ClearableEditText) view.findViewById(R.id.editText);
        editText.setOnClickListener(this);

//        Button deleteButton = (Button) view.findViewById(R.id.button);
//        deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//            }
//        });
        ImageButton addButton = (ImageButton) view.findViewById(R.id.imageButton);
        addButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        try {
            database.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        database.close();
        super.onPause();
    }

    public void hideSoftKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        word = null;
        if (listView.getAdapter().getCount() > 0) {
            word = (Word) listView.getAdapter().getItem(position);
            Intent i = new Intent(view.getContext(), TalkActivity.class);
            i.putExtra("spanish", word.getSpanish());
            i.putExtra("english", word.getEnglish());
            i.putExtra("ukrainian", word.getUkrainian());
            i.putExtra("russian", word.getRussian());
            startActivity(i);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
        word = null;
        if (listView.getAdapter().getCount() > 0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage(R.string.dialog_delete_word)
                    .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            word = (Word) listView.getAdapter().getItem(position);
                            database.deleteWord(word);
                            customListAdapter.remove(word);
                            Snackbar.make(view, R.string.word_delete, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    })
                    .setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    }).show();
//                    Toast.makeText(view.getContext(), R.string.word_delete, Toast.LENGTH_LONG).show();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageButton:
                hideSoftKeyboard();
                word = null;
                Translate translate = new Translate();
                ArrayList<String> words = translate.translate(editText.getText().toString().replace(" ", "+"));
                // save the new word to the database
                if (database.searchWord(words.get(0))) {
                    word = database.createWord(words.get(0), words.get(1), words.get(2), words.get(3));
                    customListAdapter.insert(word, 0);
                    Snackbar.make(v, R.string.word_added, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    listView.smoothScrollToPosition(customListAdapter.getPosition(word));


//                    listView.setAdapter(adapter);
//                    Toast.makeText(view.getContext(), R.string.word_exist, Toast.LENGTH_LONG).show();
                    Snackbar.make(v, R.string.word_exist, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                customListAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 1:
                typeOfSort = database.sortBySpanishWords();
                customListAdapter.clear();
                customListAdapter.addAll(typeOfSort);
                break;
            case 2:
                typeOfSort = database.sortByEnglishWords();
                customListAdapter.clear();
                customListAdapter.addAll(typeOfSort);
                break;
            case 3:
                typeOfSort = database.sortByUkrainianWords();
                customListAdapter.clear();
                customListAdapter.addAll(typeOfSort);
                break;
            case 4:
                typeOfSort = database.sortByRussianWords();
                customListAdapter.clear();
                customListAdapter.addAll(typeOfSort);
                break;
            case 5:
                typeOfSort = database.getAllWords();
                customListAdapter.clear();
                customListAdapter.addAll(typeOfSort);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
