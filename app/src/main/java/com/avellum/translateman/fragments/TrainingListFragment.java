package com.avellum.translateman.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avellum.translateman.R;

/**
 * Created by niklpod on 30.01.16, 2:50
 */
public class TrainingListFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.training_list_fragment, container, false);
        return view;
    }
}
